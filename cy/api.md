﻿售后api:  http://api.cydriveparts.com/
端口: 80

api 调用方式 和 行运兔  一致


 一. URL与参数规范
 1. URL 标准HTTP协议 规范 测试 地址 http://127.0.0.1
 	比如: http://127.0.0.1/get?module=CmdService.send&cmds=11&code=1,1
 2. path
 	1) /get 
 	2) /post
 	3) /batch, 使用modules = CmdService.send|UserService.get,  多个服务调用api
	--开放API, 无需登录 即无需cookie中的token, 只需要agent
 	4) /open/get
 	5) /open/post
 	6) /batch
 
 
3. 参数与cookie

	1) 必须 cookie 包括 token, agent[Web|And|Ios] 
 	2) 必须 form提交参数 module|modules, 表示调用的那个模块的api

4. 返回结果 json格式  {state:"000", msg: "",	data: Object }

	1) state, 000-成功, 001-失败
	2) msg 	返回的提示消息, 可能为空
	3) data 返回的数据内容, 可能为空, 格式是 常用数据类型 + 链表 + hashmap 其中之一 
	


 二. module列表
 
 备注: 所有的 openAPI 都需要 agent cookie, 所有的非openAPI 都需要 agent 和 token cookie
 
1. UserService 模块

 	1)register 建议与反馈
 	
 		cookie[agent], module = UserService.register
 		form参数: tel, password
 		
 	2)loginBypwd
 	
 		cookie[agent], module = UserService.loginBypwd
 		form参数: tel-手机号码, password-密码
 		resp返回:type 0 普通用户 1 售后 2 经销商
 
 	3)userinfo 获取用户信息
 	
 		cookie[agent,token], module = UserService.userinfo
 		form参数: 无
 		
 	4)userinfo 删除账号
 	
 		cookie[agent,token], module = UserService.delAcc
 		form参数: 无
 	
	5)postUserinfo 提交用户信息
	
 		cookie[agent,token], module = UserService.postUserinfo
 		form参数: fname-firstname, lname -lastname, company -公司名称
 		
	6)getTabimg 首页轮播图
	
 		cookie[agent,token], module = UserService.getTabimg
 		form参数: 无
 		resp返回: data 里面按 , 分隔 /tabimg/3.png, /tabimg/2.png, /tabimg/1.jpg
 		
 2. DeviceService 模块
 
 	1)getCodeInfo 二维码信息
 	
		cookie[agent], module = DeviceService.getCodeInfo
 		form参数: code
 		resp返回: codeid-返回二维码的id, scnt -扫描次数, rcnt-维修次数, b.ctime -生产时间
 		
	2)postRepairs 提交维修信息
	
		cookie[agent], module = DeviceService.postRepairs
 		form参数: codeid -扫描返回的二维码id  img-图片路径 des-描述, tel-手机号码, company-工厂
 		resp返回: 
 		
	3)repairsList 维修列表-包括 维修中 , 已完成, 类型, 时间
	
		cookie[agent], module = DeviceService.repairsList
 		form参数: state-0,1维修中,2完成   type-1,2,3,4,5,   begintime - 字符串 yyyy-mm-dd
 		resp返回: 具体看数据 state-0,1维修中,2完成
 		
 		
	4)repairsInfo 单个维修信息
	
		cookie[agent], module = DeviceService.repairsInfo
 		form参数: rid
 		resp返回: 具体看数据  state-0,1维修中,2完成
 	
 	4)repairdetail 单个维修信息
 	
		cookie[agent], module = DeviceService.repairdetail
 		form参数: rid
 		resp返回: scnt -扫描次数, rcnt-维修次数, b.ctime -生产时间  state-0,1维修中,2完成
 		
	4)doneRepairs 完成维修
	
		cookie[agent], module = DeviceService.doneRepairs
 		form参数: rid
 		resp返回: 具体看数据
 		
	4)--过期方法 repairper 维修次数
	
		cookie[agent], module = DeviceService.repairper
 		form参数: 无
 		resp返回: 具体看数据  percent 比例,非百分比
 		
	4)repairper1 个人维修次数
	
		cookie[agent], module = DeviceService.repairper1
 		form参数: 无
 		resp返回: uid,   rearaxle,motor,controller,parts,other --- 值为维修次数
 		
 		
 	文件上传
   	3. http://127.0.0.1/upload?folder=repair  -- 这个传统form表单文件上传
   	
  		POST方法调用, 
  		form参数: folder-要上传的文件夹名称,使用repair
  		body: 标准form-文件上传方式, multipart/form-data 这种
 		
 	
	