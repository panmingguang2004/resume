## brokerserver RPC 最终一致 service 负载均衡
  
- 保证到达消息服务, 涉及重发, 接口都必须保持幂等  
- 可简化为 异步的 保证到达的 rpc 框架, 此时rpc框架 检查 pre commit timeout 重发, 
- 需要 分布式事务的 添加 TranID 保证 事务的 幂等性  
- 序列化协议:
  Protostuff
  https://blog.csdn.net/caodongfang126/article/details/78751027
  kryo
  https://www.jianshu.com/p/cdd9f9bc9501
  
  
			  							(confirm resp) TCP
                              + -----------<<<---------------- +
                              |                                 |
                              + -------- +                      +  C (listener, 监听消息)
           p -HTTP | TCP-- >  |  server  |   --> MQ|异步RPC --->  +  C (listener, 监听消息)  
                   |          + -------- +                      +  C (listener, 监听消息)
                   |                 |             
                   +pre_recheck_task | commit_resend_task
                                     |
                                     + (pre commit timesover)
                                     DB(Mysql)
			  				
	server 端 如果是外网接入, 使用DNS轮询, 如果内网使用 HAProxy做HA, 无需再使用负载均衡, server本身是一个负载均衡器(相当于nginx)
		  				

## 流程梳理

- P 生产者, 调用方
- C 消费者, 业务方
- server, 中间服务

- P端调用流程
	
	p.rpc.prepareId()   提交预提交事务 prepared
	p.service.commit();  本地事务提交
	p.rpc.send&comit(); // 提交远程事务为commit

- C端流程
	
	Mq.listener {  //消息监听, 根据业务 执行 
		c.service.commit()
		c.rpc.confirm()
	}
	
- Server端
	
	根据全局事务 transId 
	1. prepared 本地存库返回
	2. commit 本地修改状态
	3. 根据业务 Queue send 到指定的 C端
	4. 等待C端返回 confirm 完成一个事务流程
	5. 检查与约定接口 
		 preCheck commitCheck timesoverCheck
		
	

## 1. server 提供http接口或tcp 长连接接口
- server.prepareId 	预提交 事务为预提交状态

	String transId = dbOption.getTransId();
	obj.put("transId", transId);
	db.insert(obj);

- server.send&commit 发送＆commit 事务为已提交状态
	
	String transId = (String) obj.get("transId");
	commit(transId);
	mqSender.send(obj); // 消息中间件发送, 根据指定不同的 queue

- server.confirm 修改事务id 状态 事务为提交确认状态

	db.updateConfirm(transId);


## MqSender 消息中间件发送

- mq.send(obj)  可以根据不同的业务 发送到相关的 服务上


## DB操作(分库) 提供事务id状态表, 用来检查pre, 重发 commit

- db.getTransId(); 获取全局事务id
	
- db.insert(Object obj); 全局事务事件保存
	
- db.updateCommit(String transId); 修改状态 commit
	
- db.updateConfirm(String transId); 修改状态 confirm
	
- db.update(String transId, Object obj); 预留

- db.getCommitList(); 获取所有已提交 未 确认
	
- db.getPreList();   获取pre 状态列表
	
- db.getTimeoutList();  多次尝试失败的


## checker 定时任务检查器

- preCheck(); 检查预提交状态, 
	同时去P端 根据TransId 获取值全局事务状态,如果成功 改为commit
	
- commitCheck(); 检查提交状态, 主动发送MQ消息到 执行的 消费端, 知道成功 confirm
	
- timesoverCheck(); 多次失败的 人工介入
	

