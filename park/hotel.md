api地址: https://gitlab.com/panmingguang2004/resume/-/blob/master/park/hotel.md

## 酒店时间段优惠减免

线上统一调用地址带签名: https://xxxxx/hotel/pushorder
	
	Method: Post
	Content-Type: application/json


###  签名方式 secret,  parkId 每个车场不同 具体询问

	sign = MD5(parkId + content + secret + timestamp)
	
###  推送车牌和住店时间

请求Json参数:
	
	{
		  "timestamp" : 11234354662, //毫秒
		  "sign":  "lsjdlfetiydfhjsdf"// 签名
		  "content" : "{}"
	}
	
	content {
		plate: '', //车牌
		startTime: '', // 住店时间 yyyyMMddHHmmss
		endTime: '',  //离店时间
		info: ''   // 酒店信息(名称), 备注用
	}
	
		
请求返回: 

	{
	  "resCode": "0", // 0 在场接收优惠成功,  1不在场优惠失败，
	  "resMsg": "车辆在场" 
	}


