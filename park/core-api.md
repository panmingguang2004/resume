api地址: https://gitlab.com/panmingguang2004/resume/-/blob/master/park/core-api.md

线上统一调用地址带签名: https://api.citypark.com.cn/Core/mqttauthservice
	
	Method: Post
	Content-Type: application/x-www-form-urlencoded


###  签名方式 使用两次MD5加密方式

	sign = MD5(appid + secret + timestamp)
	sign = MD5(sign + data + key + timestamp)
	
	public static Map<String, String> before(String data) {
			Map<String, String> headers = new HashMap<>();
			String appid = "cp123456";
	    	String timestamp = "123456";
	    	String secret = "123456";
	    	String key = "123456";
	    	String sign = MD5.encryptStrUpper(appid + secret + timestamp);
	    	sign = MD5.encryptStrUpper(sign + data + key + timestamp);
	    	headers.put("appid", appid);
	    	headers.put("sign", sign);
	    	headers.put("timestamp", timestamp);
	    	return headers;
	}
	
	  public  void addusercar(){
		HashMap<String, Object> map = new HashMap<>();
		map.put("parkId", parkId);
		map.put("username", "潘明光1");
		map.put("rentcnt", "1");
		map.put("plate", "苏D615V0,苏D615V1");
		map.put("phone", "13775237266");
		map.put("starttime", "2020-07-09 12:05");
		map.put("endtime", "2020-07-09 12:05");
		map.put("should", "0");
		map.put("paid", "0");
		map.put("type", "1");
		map.put("module", "UserCarRService.addUserCar");
		
		String data =JacksonUtil.write2JsonStr(map) ;
		before(data);
		 
		HashMap<String, Object> params = new HashMap<>();
	    params.put("data", data);
		
		Map<String, Object> resp = HttpClientUtils.post(baseUrl + "/mqttauthservice", params, headers);
		System.out.println(resp);
	}

   



文档索引
========

- [车辆是否在场](#车辆是否在场)
- [付费回调](#付费回调)
- [根据入口出口设备生成虚拟车牌](#根据入口出口设备生成虚拟车牌)
- [根据出口设备查车牌号](#根据出口设备查车牌号)
- [根据车牌查费](#根据车牌查费)
- [月租车](#月租车)
- [访客预约](#访客预约)



### 车辆是否在场

请求data Json参数:
	
	{
		  "module" :"OrderRService.carInstock",
		  "plateNo": "沪A00001",// 车牌号
		  "parkID": "2000004"// 停车场 id
	}
		
请求返回: 

	{
	  "resCode": "0", // 0 在场, 1不在场，
	  "resMsg": "车辆在场"
	}



### 付费回调

请求data Json参数:
	
	{
		  "module" :"OrderRService.paynotify",
		  "order": "20180412154545746238",// 订单编号
		  "amount": "26",// 支付金额, 单位 分。请把应缴金额 payable 回传，帕客不记录优惠信息
		  "discount": "0",// 优惠金额，请传0，帕客不记录优惠信息
		  "payType": "2",// 支付类型 1-支付宝 2-微信
		  "parkingTime": "127", // 停车时长，单位分钟
		  "onlineFreeMinute": 0, // 线上缴费减免时长  单位 分钟
		  "onlineFreeMoney": 0.01, // 线上缴费减免金额 单元 元  2位小数
		  "checkKey": "123456" // 用户 id，解决不同用户同一个车牌重复缴费的问题
		}
		
		
请求返回: 

	{
	  "resCode": "0", // 0 正确，10001 已缴费，非0，110001  为异常
	  "resMsg": "查询费用成功。",// 响应消息内容
	  "data": {
	    "elapsedTime": 127  // 停车时长
	  }
	}	


### 根据入口出口设备生成虚拟车牌

请求data Json参数:

	{
	  "module" :"OrderRService.getNoCarPlate",
	  "userId":"", // 用户ID,可以用手机号或者微信支付宝openid等绑定虚拟车牌
	  "deviceID": "4",// 入口或出口设备 id
	  "parkID": 2000004 // 帕客平台停车场 id 
	}

请求返回

	{
	  "success": true,
	  "message":"",
	  "data": {
	    "nocarFlag": false,// 正确时为 false，为true（success为false）时 没有待通行的无牌车
	    "plate":"U123456",// 虚拟车牌
	    "recordID": 100, // 进出场记录 id
	    "recordTime": "2018-11-05 00:00:00" // 进出场时间
	    ]
	  }
	}

### 根据出口设备查车牌号

请求data Json参数:

	{
	  "module" :"OrderRService.getPlateByDevice",
	  "deviceID": "4",// 出口设备 id
	  "parkID": 2000004 // 帕客平台停车场 id 
	}

请求返回

	{
	  "resCode": "0", // 0 正确，-1 为无牌车， 其他 为异常
	  "resMsg": "查询成功。",// 响应消息内容
	  "data": {
	    "plate": "沪A00001" // 车牌号码
	  }
	}
	



### 根据车牌查费

请求data Json参数:

	{
	  "module" :"OrderRService.queryfee",
	  "plateNo": "沪A00001",// 车牌号码
	  "parkID": 2000004, // 帕客平台停车场 id 
	  "onlineFreeMinute": 5, // 线上缴费减免时长  单位 分钟
	  "onlineFreeMoney": 5.20 // 线上缴费减免金额 单元 元  2位小数
	}


请求返回:
		
		{
		  "resCode": "0", // 0 正确，非0 为异常
		  "resMsg": "查询费用成功。",// 响应消息内容
		  "data": {
		    "orderNo": "20180412134002325700",// 订单编号
		    "plate": "沪A00001", // 车牌号码
		    "entryTime": "2018-04-12 13:39:34",// 入场时间
		    "payTime": "2018-04-12 13:40:07",// 预支付时间
		    "elapsedTime": 1, // 停车时长 单位分钟
		    "timeout":1, // 超时离场时间 单位  分钟
		    "payable": 1, // 停车费用, 单位 分
		    "freeMoney": 0 // 实际减免费用 单位 分
		  }
		}




###  月租车

请求data Json参数:
* 获取分组和月租规则

| 参数名称    | 参数类型 | 备注            | 是否必须 |
|-------------|----------|-----------------|---------|
| module      | string   | UserCarRService.getRule| 是  |
| parkId      | string   | 车场Id       | 是      |

返回信息:

	{
		data : {
			ruleList : [ {
				ruleid : 11,
				rulename : 夜间包月,
				price : 100
			}, {
				ruleid : 7,
				rulename : 商管员工收费规则,
				price : 100
			}],
			groupList : [{
				usergroupid : 14,
				usergroupname: 商管员工组
			}, {
				usergroupid : 19,
				usergroupname : 商户员工组
			}]
		}, resCode : 0, resMsg : null
	}

* 新增月租用户和车牌[两辆车的 请求两次接口,手机换号]

| 参数名称    | 参数类型 | 备注            | 是否必须 |
|-------------|----------|-----------------|---------|
| module      | string   | UserCarRService.addUserCar0[新增]| 是  |
| parkId      | string   | 车场Id       | 是      |
| username     | string   | 姓名  |是|
| plate     | string   | 车牌号码多个车牌用,分隔   		| 是      |
| phone      | string   | 电话    		| 是      |
| starttime       |string   | 月租开始时间yyyy-MM-dd  |是	      |
| endtime       | string   |   月租结束时间yyyy-MM-dd  | 是      |
| usergroupid       | int   | 分组id  | 是      |
| ruleid       | int   | 规则id  | 是      |
| should       | double   |   应付  | 是      |
| paid       | double   |   实付  | 是      |


* 续费月租

| 参数名称    | 参数类型 | 备注            | 是否必须 |
|-------------|----------|-----------------|---------|
| module      | string   | UserCarRService.upUserCar[续费]| 是  |
| parkId      | string   | 车场Id       | 是      |
| plate     | string   | 车牌号码多个车牌用,分隔   		| 是      |
| phone      | string   | 电话    		| 是      |
| starttime       |string   | 月租开始时间yyyy-MM-dd  |是	,未过期传空      |
| endtime       | string   |   月租结束时间yyyy-MM-dd  | 是      |
| should       | double   |   应付  | 是      |
| paid       | double   |   实付  | 是      |


* 月租信息

| 参数名称    | 参数类型 | 备注            | 是否必须 |
|-------------|----------|-----------------|---------|
| module      | string   | UserCarRService.userCarInfo| 是  |
| parkId      | string   | 车场Id       | 是      |
| plate     | string   	| plate1,plate2  	| 是      |

返回信息

	{
		data : [{
			username : 潘明光,
			usergroupname : 夜间包月用户,
			plate : 苏D615V0,
			phone : 13188888888 ,
			parkingspacecode : V0091,
			validstarttime : 2020 - 07 - 09 00: 00: 00,
			validendtime : 2020 - 07 - 09 00: 00: 00,
			price : 200
		}], resCode : 0, resMsg : null
	}
 	

----------------------

| 参数名称    | 参数类型 | 备注            | 是否必须 |
|-------------|----------|-----------------|---------|
| module      | string   | UserCarRService.addUserCar| 是  |
| parkId      | string   | 车场Id       | 是      |
| username     | string   | 姓名  |是|
| rentcnt     | string   | 租车位数  |是|
| type     | string   | 1-出租,2产权  |是|
| plate     | string   | 车牌号码多个车牌用,分隔   		| 是      |
| phone      | string   | 电话    		| 是      |
| starttime       |string   | 月租开始时间       |是	      |
| endtime       | string   |   月租结束时间  | 是      |
| should       | string   |   应付  | 是      |
| paid       | string   |   实付  | 是      |

新增和修改时所有字段为必须, 删除 parkId,plate,phone为必须
一个车位一个车牌, 车位数 = 车牌数,  如果多个车牌只有一个或两个车位, 注意车位数

请求返回:
		
		{
		  "resCode": "0", // 0 成功，1失败
		  "resMsg": "", //提示消息
		  "data": {}
		}
		

###  访客预约

请求data Json参数:

| 参数名称    | 参数类型 | 备注            | 是否必须 |
|-------------|----------|-----------------|---------|
| module      | string   |VipOrderRService.orderParkByvip      | 是      |
| mallId      | string   | 商场id-固定随便       | 是      |
| orderNo     | string   | 订单id(type变更时订单不变)   |是|
| plateNo     | string   | 车牌号码    		| 是      |
| parkId      | string   | 车场id    		| 是      |
| time       | string   | 预约截止时间2019-08-13 10:41:00	        |是	     |
| type       | string   |  1：预约成功，2：预约延长，3：取消预约  | 是      |
	

请求返回:
		
		{
		  "resCode": "0", // 0 成功，1失败
		  "resMsg": "", //提示消息
		  "data": {}
		}

