api地址: https://gitlab.com/panmingguang2004/resume/-/blob/master/park/mobile-api.md

线上地址: https://openapi.citypark.com.cn/api/

测试地址: https://dev.citypark.com.cn/api/

文档索引
========
- [签名方式](#签名方式)
- [vip车位预约](#vip车位预约)



###  签名方式
使用两次MD5加密方式

sign = MD5(appid + secret + timestamp)

sign = MD5(sign + data + key + timestamp)

		   public static Map<String, String> before(String data) {
	    		Map<String, String> headers = new HashMap<>();
	    		String appid = "cp123456";
	 	    	String timestamp = "123456";
	 	    	String secret = "123456";
	 	    	String key = "123456";
	       	 	String sign = MD5.encryptStrUpper(appid + secret + timestamp);
	        	sign = MD5.encryptStrUpper(sign + data + key + timestamp);
	        	headers.put("appid", appid);
	       	 	headers.put("sign", sign);
	        	headers.put("timestamp", timestamp);
	        	return headers;
	    }


###  vip车位预约
urL : https://dev.citypark.com.cn/api/orderparkbyvip

请求data 参数:

| 参数名称    | 参数类型 | 备注            | 是否必须 |
|-------------|----------|-----------------|---------|
| mallId      | string   | 猫酷商场id       | 是      |
| orderNo     | string   | 订单id(type变更时订单不变)   |是|
| plateNo     | string   | 车牌号码    		| 是      |
| parkId      | string   | 车场id    		| 是      |
| time       | string   | 预约截止时间2019-08-13 10:41:00	        |是	      |
| type       | string   |   1：预约成功，2：预约延长，3：取消预约  | 是      |
	
		{
		  "mallId": "0", // 猫酷商场id
		  "orderNo": "10008122224",  //订单id(type变更时订单不变)
		  "plateNo": "苏D28850", //车牌号码 
		  "parkId": "2000058", // 车场id  
		  "time": "2019-08-13 10:41:00", // 预约截止时间2019-08-13 10:41:00
		  "type": "1" //1：预约成功，2：预约延长，3：取消预约
		}


请求返回:
		
		{
		  "resCode": "0", // 0 成功，1失败
		  "resMsg": "", //提示消息
		  "data": {}
		}

