api地址: https://gitlab.com/panmingguang2004/resume/-/blob/master/park/tld.md

## 推送优惠券

线上统一调用地址带签名: URL + /pushcoupon,  URL每个车场不同, 车场提供
	
	Method: Post
	Content-Type: application/json


###  签名方式 MD5加密小写, parkid和SIGN_KEY车场提供

	sign = MD5(parkid + "_"+ plate +"_" + SIGN_KEY )
	
###  推送车牌和减免时长

请求Json参数:
	
	{   
	    "plate":"苏D615VV",
		"sign":"c55192acdd0a65a3108d8410f73596b9",    // MD5(parkid +"_"+plate+"_"+SIGN_KEY)
		"freeMoney":0.0,    // 减免金额, 填0.0
		"freeMinutes":120,  // 减免时常 , 单位分钟
		"source":"星星充电减免" //充电方品牌名称
	}
	
		
请求返回: 

	{
	  "status": "0", //  0-成功, 1失败
	  "msg": "签名错误"   // 失败看原因
	}



### 平台转发接口

固定地址URL: https://api.citypark.com.cn/Core/mqttservice

	Method: Post
	Content-Type: application/x-www-form-urlencoded

请求参数

| 参数名称    | 参数类型 | 备注            | 是否必须 |
|-------------|----------|-----------------|---------|
| module      | string   | UserCarRService.pushCoupon| 是  |
| parkId      | string   | 车场Id          | 是      |
| plate      | string   | 车牌          | 是      |
| freeMinutes | string   | 减免时长 120分钟     |是|
| freeMoney   | string   | 减免金额填0.0  	| 是      |
| source      | string   | 充电方名称     | 是      |
| sign        | string   | 签名 MD5(parkid +"_"+plate+"_"+SIGN_KEY) | 是      |


返回 
	
	{resCode=1, resMsg=签名错误}
	{resCode=0, resMsg=} //成功


