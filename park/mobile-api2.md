
## 寻车api

###  签名方式 URL和签名参数咨询提供
使用两次MD5加密方式

sign = MD5(appid + secret + timestamp)
sign = MD5(sign + data + key + timestamp)

	public static Map<String, String> before(String data) {
			Map<String, String> headers = new HashMap<>();
			String appid = "cp123456";
	    	String timestamp = "123456";
	    	String secret = "123456";
	    	String key = "123456";
	    	String sign = MD5.encryptStrUpper(appid + secret + timestamp);
	    	sign = MD5.encryptStrUpper(sign + data + key + timestamp);
	    	headers.put("appid", appid);
	    	headers.put("sign", sign);
	    	headers.put("timestamp", timestamp);
	    	return headers;
	}
	
	  public  void reversecar(){
		HashMap<String, Object> map = new HashMap<>();
		map.put("parkID", parkId);
		map.put("plate", "苏D615V0");
	
		String data =JacksonUtil.write2JsonStr(map) ;
		before(data);
		 
		HashMap<String, Object> params = new HashMap<>();
	    params.put("data", data);
		
		Map<String, Object> resp = HttpClientUtils.post(baseUrl + "/reversecar", params, headers);
		System.out.println(resp);
	}


###  反向寻车
URL : http://221.224.111.108:7070/park/openapi/reversecar

	appid: CP02278250435648304058701286765364
	secret: CPrOIONXFfoaDjSSF0MxKI8RJcstuYWfKymQp6eILcwLm0lhmRcS94T1fL9VYY
	key: CPE90ri3hPK0pxDOlV2J
	parkid: 2000083

请求data 参数:

| 参数名称    | 参数类型 | 备注            | 是否必须 |
|-------------|----------|-----------------|---------|
| plate      | string   |    车牌     | 是      |
| parkID     | string   |  车场id   |是|


请求返回:

	{
	  "success": true,
	  "data": {
	    "parkID": 2000004,// 停车场 id 和入场保持一致
	    "list": [
	      {
	        "floor":"B1",// 楼层
	        "spacecode": "D186",// 车位编号
	        "plate": "京A99998",// 车牌号码
	        "stoptime": "2018-03-23 10:10:10",// 停车时间
	        "stopid": 4 // 停车记录 id，用于查询图片使用
	      }
	    ]
	  }
	}

## stopid 找图片

URL : http://221.224.111.108:7070/park/openapi/reversecarimage?stopID=1

直接返回图片流, 不需要签名


