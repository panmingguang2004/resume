api地址: https://gitlab.com/panmingguang2004/resume/-/blob/master/park/core-api2.md

 调用地址带签名(测试):

 appid  parkid  secret  key 我方提供
	
	Method: Post
	Content-Type: application/x-www-form-urlencoded


###  签名方式 使用两次MD5加密方式

	
	sign = MD5(appid + secret + timestamp)
	sign = MD5(sign + data + key + timestamp)
	
	// 使用参数看这里
    String baseUrl = "https://openapi.citypark.com.cn/api";    // http://58.216.204.162:7071/park/openapi/
    String parkId = "2000004";
    String appid = "cp1523520634979";
    String timestamp = "123123";
    String secret = "1523520634979";
    String key = "1523520634979";

	
	public static Map<String, String> before(String data) {
			Map<String, String> headers = new HashMap<>();
			String sign = MD5.encryptStrUpper(appid + secret + timestamp);
	    	sign = MD5.encryptStrUpper(sign + data + key + timestamp);
	    	headers.put("appid", appid);
	    	headers.put("sign", sign);
	    	headers.put("timestamp", timestamp);
	    	return headers;
	}
	
     public void query() throws Exception {   // 返回的也是payable 去掉扣减的 返回单位是分
        String data = "{\"plateNo\":\"苏A00003\",\"parkID\":"+parkId+"," + "\"onlineFreeMinute\":0,"
                + "\"onlineFreeMoney\":0" + "}"; //  onlineFreeMoney 单位元
        before(data);
        HashMap<String, Object> params = new HashMap<>();
        params.put("data", data);
        Object res = HttpClientUtils.post(baseUrl + "/queryfee.htm", params,headers);
        System.out.println(res);
    }

  

文档索引
========

- [根据车牌查费](#根据车牌查费)
- [付费回调](#付费回调)
- [反向寻车](#反向寻车)
- [根据入口设备生成虚拟车牌](#根据入口设备生成虚拟车牌)
- [根据出口设备查车牌号](#5根据出口设备查车牌号)
- [vip车位预约](#vip车位预约)
- [月租车](#月租车)



### 根据车牌查费

/openapi/queryfee.htm

请求data Json参数:

	{
	  "plateNo": "沪A00001",// 车牌号码
	  "parkID": 2000004, // 帕客平台停车场 id 
	  "onlineFreeMinute": 5, // 线上缴费减免时长  单位 分钟
	  "onlineFreeMoney": 5.20 // 线上缴费减免金额 单元 元  2位小数
	}


请求返回:
		
		{
		  "resCode": "0", // 0 正确，非0 为异常
		  "resMsg": "查询费用成功。",// 响应消息内容
		  "data": {
		    "orderNo": "20180412134002325700",// 订单编号
		    "plate": "沪A00001", // 车牌号码
		    "entryTime": "2018-04-12 13:39:34",// 入场时间
		    "payTime": "2018-04-12 13:40:07",// 预支付时间
		    "elapsedTime": 1, // 停车时长 单位分钟
		    "timeout":1, // 超时离场时间 单位  分钟
		    "payable": 1, // 停车费用, 单位 分
		    "freeMoney": 0 // 实际减免费用 单位 分
		  }
		}


### 付费回调

/openapi/paynotify.htm  回调失败设置补偿机制

请求data Json参数:
	
	{
		  "order": "20180412154545746238",// 订单编号
		  "amount": "26",// 实付金额, 单位 分。 查费 payable 回传
		  "discount": "0",// 传0
		  "payType": "2",// 支付类型 1-支付宝 2-微信
		  "parkingTime": "127", // 停车时长，单位分钟
		  "onlineFreeMinute": 0, // 线上缴费减免时长  单位 分钟
		  "onlineFreeMoney": 0.01, // 线上缴费减免金额 单元 元  2位小数
		  "checkKey": "123456" // 用户 id，解决不同用户同一个车牌重复缴费的问题
		}
		
		
请求返回: 

	{
	  "resCode": "0", // 0 正确，1 异常
	  "resMsg": "支付通知成功。",// 响应消息内容
	  "data": {
	    "elapsedTime": 127  // 停车时长
	  }
	}	



###  反向寻车
URL : http://221.224.111.108:7070/park/openapi/reversecar

	appid: CP02278250435648304058701286765364
	secret: CPrOIONXFfoaDjSSF0MxKI8RJcstuYWfKymQp6eILcwLm0lhmRcS94T1fL9VYY
	key: CPE90ri3hPK0pxDOlV2J
	parkid: 2000083

请求data 参数:

| 参数名称    | 参数类型 | 备注            | 是否必须 |
|-------------|----------|-----------------|---------|
| plate      | string   |    车牌     | 是      |
| parkID     | string   |  车场id   |是|


请求返回:

	{
	  "success": true,
	  "data": {
	    "parkID": 2000004,// 停车场 id 和入场保持一致
	    "list": [
	      {
	        "floor":"B1",// 楼层
	        "spacecode": "D186",// 车位编号
	        "plate": "京A99998",// 车牌号码
	        "stoptime": "2018-03-23 10:10:10",// 停车时间
	        "stopid": 4 // 停车记录 id，用于查询图片使用
	      }
	    ]
	  }
	}

## stopid 找图片

URL : http://221.224.111.108:7070/park/openapi/reversecarimage?stopID=1

直接返回图片流, 不需要签名



### 根据入口设备生成虚拟车牌

/openapi/nocarnotify

请求data Json参数:

	{
	  "userId":"", // 用户ID,可以用手机号或者微信支付宝openid等绑定虚拟车牌
	  "deviceId": "4",// 入口或出口设备 id
	  "parkID": 2000004 // 帕客平台停车场 id 
	}

请求返回

	{
	  "success": true,
	  "message":"",
	  "data": {
	    "nocarFlag": false,// 正确时为 false，为true（success为false）时 没有待通行的无牌车
	    "plate":"U123456",// 虚拟车牌
	    "recordID": 100, // 进出场记录 id
	    "recordTime": "2018-11-05 00:00:00" // 进出场时间
	    ]
	  }
	}



### 根据出口设备查车牌号

/openapi/device/plate0  返回出口的车牌号, 无牌查询返回userId绑定车牌

请求data Json参数:

	{
	  "deviceID": "4",// 出口设备 id
	  "parkID": 2000004, // 帕客平台停车场 id 
	  "userId": "" // 微信openid
	}

请求返回

	{
	  "resCode": "0", // 0 正确，-1 为无牌车， 其他 为异常
	  "resMsg": "查询成功。",// 响应消息内容
	  "data": {
	    "plate": "沪A00001" // 车牌号码
	  }
	}
	

###  vip车位预约
urL : https://dev.citypark.com.cn/api/orderparkbyvip

请求data 参数:

| 参数名称    | 参数类型 | 备注            | 是否必须 |
|-------------|----------|-----------------|---------|
| mallId      | string   | 猫酷商场id       | 是      |
| orderNo     | string   | 订单id(type变更时订单不变)   |是|
| plateNo     | string   | 车牌号码    		| 是      |
| parkId      | string   | 车场id    		| 是      |
| time       | string   | 预约截止时间2019-08-13 10:41:00	        |是	      |
| type       | string   |   1：预约成功，2：预约延长，3：取消预约  | 是      |
	
		{
		  "mallId": "0", // 猫酷商场id
		  "orderNo": "10008122224",  //订单id(type变更时订单不变)
		  "plateNo": "苏D28850", //车牌号码 
		  "parkId": "2000058", // 车场id  
		  "time": "2019-08-13 10:41:00", // 预约截止时间2019-08-13 10:41:00
		  "type": "1" //1：预约成功，2：预约延长，3：取消预约
		}


请求返回:
		
		{
		  "resCode": "0", // 0 成功，1失败
		  "resMsg": "", //提示消息
		  "data": {}
		}



###  月租车

统一调用URL:  https://api.citypark.com.cn/Core/mqttauthservice

请求data Json参数:
* 获取分组和月租规则

| 参数名称    | 参数类型 | 备注            | 是否必须 |
|-------------|----------|-----------------|---------|
| module      | string   | UserCarRService.getRule| 是  |
| parkId      | string   | 车场Id       | 是      |

返回信息:

	{
		data : {
			ruleList : [ {
				ruleid : 11,
				rulename : 夜间包月,
				price : 100
			}, {
				ruleid : 7,
				rulename : 商管员工收费规则,
				price : 100
			}],
			groupList : [{
				usergroupid : 14,
				usergroupname: 商管员工组
			}, {
				usergroupid : 19,
				usergroupname : 商户员工组
			}]
		}, resCode : 0, resMsg : null
	}

* 新增月租用户和车牌[手机和车牌一一对应,一个手机绑定一个车牌]

| 参数名称    | 参数类型 | 备注            | 是否必须 |
|-------------|----------|-----------------|---------|
| module      | string   | UserCarRService.addUserCar0[新增]| 是  |
| parkId      | string   | 车场Id       | 是      |
| username     | string   | 姓名  |是|
| plate     | string   | 车牌号码多个车牌用,分隔   		| 是      |
| phone      | string   | 电话    		| 是      |
| starttime       |string   | 月租开始时间yyyy-MM-dd  |是	      |
| endtime       | string   |   月租结束时间yyyy-MM-dd  | 是      |
| usergroupid       | int   | 分组id  | 是      |
| ruleid       | int   | 规则id  | 是      |
| should       | double   |   应付  | 是      |
| paid       | double   |   实付  | 是      |


* 续费月租

| 参数名称    | 参数类型 | 备注            | 是否必须 |
|-------------|----------|-----------------|---------|
| module      | string   | UserCarRService.upUserCar[续费]| 是  |
| parkId      | string   | 车场Id       | 是      |
| plate     | string   | 车牌号码多个车牌用,分隔   		| 是      |
| phone      | string   | 电话    		| 是      |
| starttime       |string   | 月租开始时间yyyy-MM-dd  |是	,未过期传空	      |
| endtime       | string   |   月租结束时间yyyy-MM-dd  | 是      |
| should       | double   |   应付  | 是      |
| paid       | double   |   实付  | 是      |

* 解绑删除月租

| 参数名称    | 参数类型 | 备注            | 是否必须 |
|-------------|----------|-----------------|---------|
| module      | string   | UserCarRService.delUserCar| 是  |
| parkId      | string   | 车场Id       | 是      |
| plate     | string   | 车牌号码   		| 是      |
| phone      | string   | 电话    		| 是      |


* 月租信息

| 参数名称    | 参数类型 | 备注            | 是否必须 |
|-------------|----------|-----------------|---------|
| module      | string   | UserCarRService.userCarInfo| 是  |
| parkId      | string   | 车场Id       | 是      |
| plate     | string   	| plate1,plate2  	| 是      |

返回信息

	{
		data : [{
			username : 潘明光,
			usergroupname : 夜间包月用户,
			plate : 苏D615V0,
			phone : 13188888888 ,
			parkingspacecode : V0091,
			validstarttime : 2020 - 07 - 09 00: 00: 00,
			validendtime : 2020 - 07 - 09 00: 00: 00,
			price : 200
		}], resCode : 0, resMsg : null
	}
 	

请求返回:
		
		{
		  "resCode": "0", // 0 成功，1失败
		  "resMsg": "", //提示消息
		  "data": {}
		}
		
		
		

