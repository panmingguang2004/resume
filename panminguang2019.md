# 简历地址: 

- https://gitlab.com/panmingguang2004/resume/-/blob/master/panminguang2019.md

# 联系方式

- 潘明光/男/1987
- 手机：13775237258 
- Email：panmingguang2004@gmail.com
- QQ：414072243
- 微信号：13775237258

---

# 个人信息

 - 潘明光/男/1987
 - 本科/重庆大学网络教育 计算机系科学与技术[专升本] 
 - 工作年限：10年以上
 - 婚姻状况：已婚已育
 - 交通工具：私家车
 - 国内git仓库：https://gitee.com/panmingguang2004  
 - 期望职位：java技术总工, java架构师, java scada高级开发, Java高级程序员
 - 期望薪资：税前月薪20~30k
 - 期望城市：常州

---

## 技术经验
	
#### [物联网服务架构图](https://code.aliyun.com/414072243/resume/blob/master/%E8%81%94%E7%BD%91%E7%AE%A1%E7%90%86%E5%81%9C%E8%BD%A6%E6%9C%8D%E5%8A%A1%E6%9E%B6%E6%9E%84.md)
 

#### [springboot-simple](https://gitee.com/panmingguang2004/springboot-simple)

         软件架构
	springboot + springJDBC + druidpool + velocity 最精简易上手 快速开发 框架, 专注 sql 语句
	
	build 完成目录:
	[
	bin -启动bat sh
	conf -- classpath配置文件
	lib -- 所有的jar包
	logs -- 日志
	webapp --  vm templaete, 静态文件路径
	]
	

#### [cocook](https://gitee.com/panmingguang2004/cocook)
   
     cocook-mvc  常规 servlet-api mvc框架, jdbc-temp 适配, velocity等支持,  undertow 集成boot一键打包 sh启动方式
	 cocook-io   
	 		nio aio 基础通讯框架 使用自定义协议co和通用http1.1, 参考znet等, 实现http长连接, 无业务并发, 轻松过10Wqps
	 		自定义流传输协议co,
	 		参考：https://gitee.com/panmingguang2004/cocook/blob/master/cocook-io/src/main/java/cn/tom/protocol/co/protocol-info.md
	 cocook-rpc  broker rpc, 性能强劲, 自定义规范
	 		参考:https://gitee.com/panmingguang2004/cocook/blob/master/cocook-rpc/src/main/java/cn/tom/rpc/rpc.md
	
						   +---->  serviceA[登录注册]
						   |    
			client --> borker --> serviceB[消息]
						   |
						   +---->  serviceC[业务]  
		   
    cocook-db   添加diskQueue , 实现了固态化的 diskQueue,参考：https://gitee.com/panmingguang2004/cocook/blob/master/cocook-db/src/main/java/cn/tom/queue/DiskQueue-info.md
    cocook-mq   MQ 缓存消息队列, 持久化Query, 嵌入式数据库支持, 未实现
	

#### [xyt_msg_4.0](https://gitee.com/panmingguang2004/xyt_msg_4.0)
 
	通用硬件接入框架 TCP&UDP
	使用netty 解析tcp 自定义协议
	1.0 使用springboot 开放http接口,使用同步的http协议调用
	2.0 废弃springboot 使用 cocook-io 提供外部调用长连接接口, 
	3.0,4.0 改用异步的 http接口 tcp命令调用,已解决 客户端调用卡顿问题
	手机端后台服务调用 cocook-io服务 转发 调用 tcp 命令协议
		
#### [tcp-broker-service](https://gitee.com/panmingguang2004/tcp-broker-service)
 
	TCP&UDP 协议解析器  ---[根据订阅发送] -- >   broker[中间消息转发器]  -- > service[业务处理层]
	解决　tcp流量横向扩展问题
	解决    service 业务横向扩展问题
 
### [springMVC-undertow](https://gitee.com/panmingguang2004/springmvc-undertow)
 
	改springMvc + undertow  boot 启动方式
	build 完成目录:
	 [
		 bin -启动bat sh
		 conf -- classpath配置文件
		 lib -- 所有的jar包
		 logs -- 日志
		 webapp -- 原来的 webapp 路径
	  ]
 
 
#### [springcloud-simple](https://gitee.com/panmingguang2004/springcloud-simple)

    springcloud精简项目, 
         包括eureka注册中心, 所有的服务 都会注册进来, 包括 feign ribbon
    Service提供服务端,并直接暴露http端口调用
	ribbon[客户端负载均衡器] -feign[ribbon+restTemp] - netflix[熔断机制组件]
	zuul -功能是路由转发和过滤器
	config-  配置中心,未集成
	
### [httpclient](https://gitee.com/panmingguang2004/httpclient) 
 
    常规 httpclient工具, 以前用于抓取项目, 后沿用, 多年稳定更新使用

---

# 工作经历

## 常州帕客科技有限公司(新城物业旗下) （2019年07月 ~ 至今）

- 项目职务: 技术研发
- 负责项目是: 新车吾悦停车管理系统, 包括岗亭客户端, 后台停车管理系统, 车位监控管理, 室外导视等
  主要对接 出入口相机,  车位相机监测, 导视数据推送
- 新城总部平台对接, 市政智慧平台对接等
- 新能源充电桩 对接优惠等 车场第三方服务
- 项目地址:  内网


## 存誉网络科技有限公司(倒闭) （2018年12月 ~2019年7）

- 项目职务: 技术架构, 技术总工
- 负责项目是: 在线租车平台, 客盈门, 在线抢单
- 项目地址:  https://gitee.com/cuny


## 上海行乎互联网科技有限公司 （ 2016年12月 ~ 2018年12月）

- 项目职务: 技术架构, 技术总工
- 负责项目是: 共享单车解决方案,  智能车解决方案
- 项目地址:  https://gitee.com/row_rabbit
- `给多家 三四线区城市域性 提供共享电单车解决方案, 在运营的有 艾特出行, 橙车出行, 山哈弯, 益约出行等`
- `给快递物流车提供整体观察监控解决方案[天邮智联], 杭州`
- `给电动车实现智能管理app, 定位等一键启动`
- `UDP协议硬件产品, 电动车智能充电投币器, 刷卡投币等, 智能LED显示屏等`
- `实现共享智能换电充电柜项目,协议调试完成,因硬件投入庞大,后未实施`

## 上海享骑电动车服务有限公司  （ 2015年9月 ~ 2016年12月 ）

- 项目职务: 技术架构, 技术总工
- 负责项目: 享骑电单车APP, 智能锁设备服务, 享骑后台服务端开发
- `享骑电单车 起步同于摩拜, 当时与摩拜一同上市运营, 老板非互联网企业, 无融资`
- `扩张时月流水千万, 运营管理混乱, 损坏严重, 后退出舞台, 变卖盛大`

## 常州数赢云网络科技有限公司   （2013年3月 ~ 2015年8月）
	
- 项目职务: 开发组长 
- 负责项目: 分布式数据抓取系统，`当时抓58 赶集等内容发布网站, 单日抓取量超1000W, 上百台VPS同时抓取`
- QQ分布式聊天破解，`破解 WEBQQ 实现一键100个QQ 一键登录, 后台群发, 单对单聊天, 后腾讯取消webQQ, 退出舞台`
- 享推APP

## 常州银科金典   （ 2011年3月-2013年3月 ）
- 项目职务: 开发
- 负责项目: 银行考核系统，信贷系统，对账单系统 
- `离职时已单独负责考核系统 和 对账单系统`
---

# 技能清单

以下均为我熟练使用的技能

- java关键词：Spring NIO ThreadPool Cache Invoker Controller Router ClassLoader
- 前端框架：Bootstrap/VUE/Jquery
- 数据库相关：PgSQL/MySQL/MongoDB/Redis
- 版本管理、文档和自动化部署工具：Svn/Git/Maven
- 应用服务器: undertow tomcat jetty resin
- 熟练linux服务, 全部使用linux服务器部署


# 致谢
感谢您花时间阅读我的简历，期待能有机会和您共事。

